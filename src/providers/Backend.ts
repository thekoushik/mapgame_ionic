import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export default class Backend{
    private user;
    private gameinfo;
    private user_lat;
    private user_lng;
    private static IP="http://ec2-18-222-104-136.us-east-2.compute.amazonaws.com:8000/api/";

    constructor(private http:Http){}

    public setUserInfo(info){
        this.user=info;
    }

    public setUser(user){
        this.gameinfo=user;
        this.user_lat=user.ship_lat;
        this.user_lng=user.ship_lng;
    }
    public getDifference(){
        return this.gameinfo.reach_difference;
    }
    public getTarget(){
        if(this.gameinfo.reach_difference)
            return {
                lat:this.gameinfo.target_lat,
                lng:this.gameinfo.target_lng
            };
        else
            return null;
    }
    public setUserLocation(lat,lng){
        this.user_lat=lat;
        this.user_lng=lng
    }
    public getUserLocation(){
        return {
            lat:this.user_lat,
            lng:this.user_lng
        };
    }
    public getUser(){
        return this.user;
    }

    public saveUser(data){
        return this.http.post(Backend.IP+'signin',{
            displayName:data.displayName,
            userId:data.userId,
            lat:this.user_lat,
            lng:this.user_lng
        })
        .map(res => res.json())
        .toPromise();
    }

    public fetchUser(id){
        return this.http.post(Backend.IP+'fetch',{
            id:id
        })
        .map(res => res.json())
        .toPromise();
    }
    public fetchMe(){
        return this.fetchUser(this.user.userId);
    }
    public sendGoto(time,tlat,tlng,cur_lat=null,cur_lng=null){
        if(cur_lat){
            return this.http.post(Backend.IP+'go',{
                id:this.user.userId,
                time:time,
                tlat:tlat,
                tlng:tlng,
                clat:cur_lat,
                clng:cur_lng
            })
            .map(res => res.json())
            .toPromise();
        }else{
            return this.http.post(Backend.IP+'go',{
                id:this.user.userId,
                time:time,
                tlat:tlat,
                tlng:tlng
            })
            .map(res => res.json())
            .toPromise();
        }
    }
}