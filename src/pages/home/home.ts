import { Component } from '@angular/core';
import { IonicPage,Platform } from 'ionic-angular';
import { NavController,AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { GooglePlus } from '@ionic-native/google-plus';

import Backend from '../../providers/Backend';
import { GamePage } from '../game/game';
import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    private platform:Platform,
    private backend:Backend,
    public navCtrl: NavController,
    public alertController:AlertController,
    private storage:Storage,
    private googlePlus: GooglePlus,
    private geo:Geolocation) {
  }

  showAlert(msg,sub=""){
    this.alertController.create({
      title: msg,
      subTitle: sub,
      buttons: ['Dismiss']
    }).present();
  }

  ionViewDidLoad(){
    try{
      this.geo.getCurrentPosition().then((resp) => {
        this.backend.setUserLocation(resp.coords.latitude,resp.coords.longitude);
      })
      .catch((error) => {
        this.alertController.create({
          title: 'Error getting location',
          subTitle: 'Please allow location access.',
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.platform.exitApp();
            }
          }]
        }).present();
      });
      this.storage.get('user_id')
      .then((l)=>{
        if(l!=null){
          console.log('L',l);
          this.googlePlus.trySilentLogin({})
          .then((u)=>{
            console.log('silent',u);
            this.backend.setUserInfo(u);
            return this.backend.fetchUser(u.userId)
          })
          .then(res=>{
            console.log('fetch',res);
            this.backend.setUser(res);
            this.navCtrl.setRoot(GamePage)
          })
          .catch((err)=>{
            console.log(err);
          })
        }else{
          console.log('storage null');
        }
      })
    }catch(e){}
  }
  play(){
    this.googlePlus.login({
      //webClientId:'565293644376-a1p1h81sgkfhpjr451drkme8lqncj19q.apps.googleusercontent.com'
    })
    .then(res => {
      console.log('res',res);
      this.backend.setUserInfo(res);
      return this.backend.saveUser(res);
    })
    .then((u)=>{
      console.log('back from backend',u);
      if(u.data){
        this.backend.setUser(u.data);
        return this.storage.set('user_id',u.data.google_user_id);
      }else{
        this.backend.setUser(u);
        return this.storage.set('user_id',u.google_user_id);
      }
    })
    .then(_=>this.navCtrl.setRoot(GamePage))
    .catch(err => {
      console.error('Error',err)
      this.showAlert("Cannot Sign In");
    });
  }
}
