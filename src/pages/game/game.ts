import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, Modal, ModalController } from 'ionic-angular';
import { NavController,AlertController } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';
import { Geolocation } from '@ionic-native/geolocation';

import World from './World';
import Player from './Player.ts';
import HQ from './HQ';
import Backend from '../../providers/Backend';

import { ShopComponent } from '../../components/shop/shop';
import { LootGrabComponent } from '../../components/loot-grab/loot-grab';
import { ProfileComponent } from '../../components/profile/profile';
import { RadioComponent } from '../../components/radio/radio';
import { UpgradeModalComponent } from '../../components/upgrade-modal/upgrade-modal';

declare var google;

@IonicPage()
@Component({
  selector: 'page-game',
  templateUrl: 'game.html'
})
export class GamePage {
  
  @ViewChild('map') mapElement: ElementRef;
  //static map: any;
  username;
  userid;
  world;
  player;
  hqs;
  food;
  oil;
  targetMarker:any;
  targetLine:any;
  shopModal:Modal;

  constructor(
    private navCtrl: NavController,
    private alertController:AlertController,
    private backend:Backend,
    private googlePlus: GooglePlus,
    private geo:Geolocation,
    public modalCtrl:ModalController) {
  }

  ionViewDidLoad(){
    var user = this.backend.getUser();
    this.username=user.displayName;
    this.userid=user.userId;

    this.food=10;
    this.oil=100;
    this.targetMarker=null;
    this.world=new World();
    this.player=new Player();
    this.hqs=[];
    this.initMap();
  }

  addHQ(pos,click=null){
    var hq=new HQ();
    hq.init(pos,click);
    this.hqs.push(hq);
  }

  async initMap(){
    this.world.setMap(this.mapElement.nativeElement,this.backend.getUserLocation());
    google.maps.event.addListener(World.map, 'click', this.processMapClick.bind(this));
    google.maps.event.addListener(World.map, 'zoom_changed', ()=>{
      var zoom = World.map.getZoom();
        for (var i = 0; i < this.hqs.length; i++)
            this.hqs[i].show(zoom >= 15);
    });
    var diff=this.backend.getDifference();
    if(diff!=undefined){
      console.log('diff',diff);
      var target=this.backend.getTarget();
      this.setTarget(target);
      this.player.setGoing(diff,this.backend.getUserLocation(),target);
    }else{
      console.log('no diff');
      this.player.setMarker(World.map.getCenter());
    }
    this.addHQ({lat:22.585630,lng:88.421616},(e)=>{
      this.showAlert("Access Denied","You need to find your access module first.");
    });
    //this.addHQ({lat:40.712775,lng:-74.005973});
    this.gotoship();
  }

  gotoship(){
    this.player.centerView();
  }
  showAlert(msg,sub=""){
    let alert = this.alertController.create({
      title: msg,
      subTitle: sub,
      buttons: ['Dismiss']
    });
    alert.present();
  }
  boost(){
    if(this.oil<10){
      this.showAlert("Insufficient Oil","You need to salvage more oil.");
      return;
    }
    if(this.player.boostSpeed())
      this.oil-=10;
    else
      this.showAlert("Ship is idle","You need to select a destination first.");
  }

  /*bouncePlayer() {
    if (this.playerMarker.getAnimation() !== null) {
      this.playerMarker.setAnimation(null);
    } else {
      this.playerMarker.setAnimation(google.maps.Animation.BOUNCE);
    }
  }*/

  drawLine(pos1,pos2){
    if(this.targetLine==null)
      this.targetLine = new google.maps.Polyline({
        path: [pos1,pos2],
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2,
        map:World.map
      });
    else
      this.targetLine.setPath([pos1,pos2]);
  }
  setTarget(latlong){
    if(this.targetMarker==null){
      this.targetMarker=new google.maps.Marker({
        map:World.map,
        position:latlong,
        icon: {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 15,
            fillColor: "#0FF",
            fillOpacity: 0.4,
            strokeWeight: 3,
            strokeColor: "#00F",
            strokeOpacity: 0.5
        }
      })
    }else{
      this.targetMarker.setPosition(latlong);
    }
  }
  async processMapClick(event){
    var latlong=event.latLng;
    this.setTarget(latlong);
    //this.drawLine(this.targetMarker.getPosition(),this.playerMarker.getPosition())
    var c=this.player.goToPoint(latlong);
    var promise;
    if(this.player.isMoving()){
      var pos=this.player.getPosition();
      promise=this.backend.sendGoto(c,latlong.lat(),latlong.lng(),pos.lat(),pos.lng());
    }else
      promise=this.backend.sendGoto(c,latlong.lat(),latlong.lng());    
    promise.then((u)=>{
        this.player.startMoving();
    })
    .catch((e)=>{
        console.log('backend send error',e);
    });
  }
  /////
  logout(){
    this.googlePlus.logout()
    .then(_=>{
      this.navCtrl.setRoot('HomePage');
    })
    .catch(_=>console.log('oops'));
  }
  openShop(){
    this.shopModal = this.modalCtrl.create(ShopComponent, { a:"B" });
    this.shopModal.present();
  }
  scan(){
    //this.showAlert("Area is empty");
    this.shopModal = this.modalCtrl.create(LootGrabComponent, { a:"B" });
    this.shopModal.present();
  }
  openProfile(){
    this.shopModal = this.modalCtrl.create(ProfileComponent, { a:"B" });
    this.shopModal.present();
  }
  openRadio(){
    this.shopModal = this.modalCtrl.create(RadioComponent, { a:"B" });
    this.shopModal.present();
  }
  openUpgradeModal(){
    this.shopModal = this.modalCtrl.create(UpgradeModalComponent, { a:"B" });
    this.shopModal.present();
  }
}
