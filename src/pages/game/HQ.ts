import World from './World';
declare var google;
export default class HQ{
    marker;
    icon_config;

    constructor(){
        this.icon_config={
            url: 'assets/icon/ship/HQ.png',
            scaledSize: new google.maps.Size(300, 300),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(150, 150),
            labelOrigin: new google.maps.Point(150,150)
        };
    }
    public show(vis){
        this.marker.setVisible(vis);
    }
    public init(pos,click=null){
        this.marker=new google.maps.Marker({
            position: pos,
            map: World.map,
            icon: this.icon_config
        });
        if(click)
            this.marker.addListener('click',click)
    }
}