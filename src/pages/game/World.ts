import style_map from './style_map_3.json';

declare var google;

export default class World{
    static map;
    constructor(){
    }
    public setMap(el){
        World.map = new google.maps.Map(el, {
            zoom: 16,
            center: {lat: 22.577436, lng: 88.432710},
            disableDefaultUI: true,
            clickableIcons:false,
            styles:style_map
        });
    }
}