import World from './World';
declare var google;

export default class Player{
    readonly speed = 50; // km/h
    readonly delay = 100;

    moving=false;

    marker;
    line;

    km_h=50;
    lat;
    lng;
    step;
    dest;
    distance;
    //numStep;
    i;
    deltaLat;
    deltaLng;
    
    total_time;

    icon_config;
    boosted=false;

    constructor() {
        this.icon_config={
            url: 'assets/icon/ship/90.png',
            scaledSize: new google.maps.Size(120, 120),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(60, 60),
            labelOrigin: new google.maps.Point(60,60)
        }
        //how long 1 step should be
        this.step = (this.km_h * 1000 * this.delay) / 3600000; // in meters
    }

    public setMarker(pos){
        this.marker=new google.maps.Marker({
            position: pos,
            map: World.map,
            icon: this.icon_config
            /*label:{
                color:'#fff'
            }*/
        });
    }

    rotatePlayer(prev,next){
        var a=google.maps.geometry.spherical.computeHeading(prev, next);
        var icon=90;
        if(a<22 && a>-22)
            icon=90;
        else if(a<68 && a>22)
            icon=45;
        else if(a<114 && a>68)
            icon=0;
        else if(a<159 && a>114)
            icon=315;
        else if(a>159 || a<-159)
            icon=270;
        else if(a>-159 && a<-114)
            icon=225;
        else if(a>-114 && a<-68)
            icon=180;
        else// if(a>-68 && a<-22)
            icon=135;
        //console.log(a);
        this.icon_config.url='assets/icon/ship/'+icon+(this.boosted?"f":"")+".png";
        this.marker.setIcon(this.icon_config);
    }

    public centerView(){
        World.map.setCenter(this.marker.getPosition());
    }
    public boostSpeed(){
        if(!this.moving) return false;
        this.boosted=true;
        this.rotatePlayer(this.marker.position,this.dest);
        return true;
    }
    public setGoing(diff,from,target){
        this.setMarker(from);
        this.lat = from.lat;//this.marker.position.lat();
        this.lng = from.lng;//this.marker.position.lng();
        this.dest=new google.maps.LatLng(target.lat,target.lng);
        this.distance =google.maps.geometry.spherical.computeDistanceBetween(this.dest, this.marker.position); // in meters
        this.rotatePlayer(this.marker.position,this.dest);
        this.total_time=74*this.distance;
        var travelled=this.total_time-diff;//in milli
        console.log('travelled',travelled);
        var numStep = this.distance / this.step;
        this.i = 0;
        this.deltaLat = (this.dest.lat() - this.lat) / numStep;
        this.deltaLng = (this.dest.lng() - this.lng) / numStep;

        var n=travelled/this.delay;
        this.i=this.step*n;
        this.lat+=(this.deltaLat*n);
        this.lng+=(this.deltaLng*n);
        var delay_mod=travelled % this.delay;
        this.calcTime();
        var ltln=new google.maps.LatLng(this.lat, this.lng);
        this.marker.setPosition(ltln);
        if(!this.moving){
            this.moving=true;
            if(delay_mod<1)
                this.moveMarker();
            else
                setTimeout(this.moveMarker.bind(this), delay_mod);
        }
    }
    public goToPoint(dest){
        this.lat = this.marker.position.lat();
        this.lng = this.marker.position.lng();
        this.dest=dest;
        
        this.distance =google.maps.geometry.spherical.computeDistanceBetween(dest, this.marker.position); // in meters
        
        var numStep = this.distance / this.step;
        this.i = 0;
        this.deltaLat = (dest.lat() - this.lat) / numStep;
        this.deltaLng = (dest.lng() - this.lng) / numStep;

        //this.line=
        this.rotatePlayer(this.marker.position,dest);
        this.total_time=74*this.distance;

        return this.calcTime();
    }
    public getPosition(){
        return this.marker.position;
    }
    public isMoving(){
        return this.moving;
    }
    public startMoving(){
        if(!this.moving){
            this.moving=true;
            this.moveMarker();
        }
    }
    diffToStr(diff){//milli
        if(diff<500)
            return "0s";
        else if(diff<1000)
            return "1s";
        var s=Math.floor(diff/1000);
        if(s<60)
            return s+"s";
        var m=Math.floor(s/60);
        s=s%60;
        if(m<60){
            if(s<5)
                return m+"m";
            else
                return m+"m "+s+"s";
        }
        var h=Math.floor(m/60);
        m=m%60;
        if(h<24){
            if(m>0)
                return h+"h "+m+"m";
            else
                return h+"h";
        }
        var d=Math.floor(h/24);
        h=h%24;
        if(h>0)
            return d+"d "+h+"h";
        else
            return d+"d";
    }
    calcTime(off=false){
        if(off){
            this.marker.setLabel(null);
        }else{
            var diff=this.diffToStr(this.total_time-(74*this.i));//Math.floor(this.distance)-Math.floor(this.i);
            this.marker.setLabel({
                text: diff,
                color: "yellow",
                fontSize:"20px",
                fontWeight:"bold"
            });
            return diff;
        }
    }
    drawLine(pos1,pos2){
        if(this.line==null)
            this.line = new google.maps.Polyline({
                path: [pos1,pos2],
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2,
                map:World.map
            });
        else
            this.line.setPath([pos1,pos2]);
    }
    moveMarker(){
        this.lat += this.deltaLat;
        this.lng += this.deltaLng;
        this.i += this.step;
        if (this.i < this.distance){
            this.calcTime();
            var ltln=new google.maps.LatLng(this.lat, this.lng);
            this.marker.setPosition(ltln);
            setTimeout(this.moveMarker.bind(this), this.delay);
        }else{
            this.marker.setPosition(this.dest);
            this.moving=false;
            this.calcTime(true);
            this.boosted=false;
        }
    }
}