import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

/**
 * Generated class for the RadioComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'radio',
  templateUrl: 'radio.html'
})
export class RadioComponent {
  constructor(private viewCtrl:ViewController) {
  }
  closeModal(){
    this.viewCtrl.dismiss();
  }
}
