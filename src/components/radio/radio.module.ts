import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RadioComponent } from './radio';

@NgModule({
  declarations: [
    RadioComponent,
  ],
  imports: [
    IonicPageModule.forChild(RadioComponent),
  ],
  exports: [
    RadioComponent
  ]
})
export class RadioComponentModule {}
