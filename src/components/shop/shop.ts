import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
/**
 * Generated class for the ShopComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'shop',
  templateUrl: 'shop.html'
})
export class ShopComponent {
  readonly shopText=[
    "Shop",
    'Ship',
    'Base',
    'Accessories',
    'Decoration',
    'Shield',
    'Todays Offer'
  ];
  todayOffer={};
  shopMenuView=0;
  constructor(private viewCtrl:ViewController) {
  }
  backShop(){
    this.shopMenuView=0;
  }
  openShop(index){
    if(index==6){
      console.log("Buy Today's offer");
    }else{
      this.shopMenuView=index;
    }
  }
  closeModal(){
    this.viewCtrl.dismiss();
  }
}
