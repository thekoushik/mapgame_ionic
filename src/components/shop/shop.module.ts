import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShopComponent } from './shop';

@NgModule({
  declarations: [
    ShopComponent,
  ],
  imports: [
    IonicPageModule.forChild(ShopComponent),
  ],
  exports: [
    ShopComponent
  ]
})
export class ShopComponentModule {}
