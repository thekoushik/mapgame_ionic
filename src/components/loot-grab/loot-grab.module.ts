import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LootGrabComponent } from './loot-grab';

@NgModule({
  declarations: [
    LootGrabComponent,
  ],
  imports: [
    IonicPageModule.forChild(LootGrabComponent),
  ],
  exports: [
    LootGrabComponent
  ]
})
export class LootGrabComponentModule {}
