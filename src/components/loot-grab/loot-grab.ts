import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
/**
 * Generated class for the LootGrabComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'loot-grab',
  templateUrl: 'loot-grab.html'
})
export class LootGrabComponent {
  constructor(private viewCtrl:ViewController) {
  }
  closeModal(){
    this.viewCtrl.dismiss();
  }
}
