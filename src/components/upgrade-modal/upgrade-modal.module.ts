import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpgradeModalComponent } from './upgrade-modal';

@NgModule({
  declarations: [
    UpgradeModalComponent,
  ],
  imports: [
    IonicPageModule.forChild(UpgradeModalComponent),
  ],
  exports: [
    UpgradeModalComponent
  ]
})
export class UpgradeModalComponentModule {}
