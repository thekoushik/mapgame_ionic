import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

/**
 * Generated class for the UpgradeModalComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'upgrade-modal',
  templateUrl: 'upgrade-modal.html'
})
export class UpgradeModalComponent {
  constructor(private viewCtrl:ViewController) {
  }
  closeModal(){
    this.viewCtrl.dismiss();
  }
}
