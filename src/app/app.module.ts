import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { GooglePlus } from '@ionic-native/google-plus';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpModule } from '@angular/http';
import Backend from '../providers/Backend';
import { MyApp } from './app.component';

//import { GamePage } from '../pages/game/game';
import { GamePageModule } from '../pages/game/game.module';

import { ShopComponent } from '../components/shop/shop';
import { LootGrabComponent } from '../components/loot-grab/loot-grab';
import { RadioComponent } from '../components/radio/radio';
import { ProfileComponent } from '../components/profile/profile';
import { UpgradeModalComponent } from '../components/upgrade-modal/upgrade-modal';

@NgModule({
  declarations: [
    MyApp,
    ShopComponent,
    LootGrabComponent,
    RadioComponent,
    ProfileComponent,
    UpgradeModalComponent,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    GamePageModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ShopComponent,
    LootGrabComponent,
    RadioComponent,
    ProfileComponent,
    UpgradeModalComponent,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Backend,
    GooglePlus,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
